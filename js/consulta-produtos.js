$( document ).ready(function() {
	var bestPictures = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
		  url: 'consultas_typeahead_ajax.php?q=%QUERY',
		  wildcard: '%QUERY'
		}
	});

	$('#remote .typeahead').typeahead(null, {
		name: 'best-pictures',
		display: 'value',
		source: bestPictures,
		limit: 10
	})
	.bind('typeahead:selected', function(obj, selected, name) {
		window.location.href = "consulta_produtos.php?id="+selected.id;
    return false;
  })
  .off('blur')
  .on('typeahead:asyncrequest', function() {
    $('.Typeahead-spinner').show();
  })
  .on('typeahead:asynccancel typeahead:asyncreceive', function() {
    $('.Typeahead-spinner').hide();
  });
});
