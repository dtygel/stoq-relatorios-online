<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<title>Consulta de produtos</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<script src="js/jquery-2.1.4.min.js"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/typeahead.css">
	<script src="js/bootstrap.min.js"></script>
	<script src="js/typeahead.js"></script>
	<?php
		if (isset($javascripts)) {
			foreach ($javascripts as $js) {
				?>
					<script src="js/<?= $js ?>.js"></script>
				<?php
			}
		}
	?>
	<link rel="stylesheet" href="css/style.css">
</head>
<body style="padding-top:150px">

<?php
include("config.php");
include("functions.php");
setlocale(LC_TIME, "pt_BR.UTF-8");
include("connect.php");

?>

