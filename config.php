<?php

$oculta_fornecedor = false;
$rows_per_page = 9;
$min_year = 2015;
$max_year = date('Y');

$fornecedores_vetados = array(
	"Sadi do mel",
	"Artefatos de pano",
	"AAFASD - Associação dos Agricultores Familiares do Assentamento Santo Dias ",
	"Gilmar",
	"Fazenda Cachoeira",
	"Fazenda São Lucas do Jurumin",
	"Marcela do Pão de Mel",
	"Terra Mãe",
	"Feirinha de Santana no Mercado Municipal", 
	"Neide Carvalho - Ovos Caipiras", 
	"Priscila do Egídio (Bom Retiro)",
	"Selma Generoso",
	"Gourmet da Horta",
	"Vera e Wilson",
	"Pura Vida",
	"Papel Pinel",
	"Art Gravatá",
	"Feito Flor",
	"CIMQCB- Cooperativa Interestadual das Mulheres Quebradeiras de Coco Babacu",
	"Sukaina e Claret",
	"Fazenda Alegria",
	"Ateliê Oráculo",
	"Lucia Reis",
	"Antônia da Glória Fernandes Moreira",
	"Camila do Maranhão",
	"Rita e Nem"
);

?>
