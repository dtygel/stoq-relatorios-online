<?php

include('template/header.php');

$sel_mes = (isset($_GET["mes"]))
	? sprintf('%02d', $_GET["mes"])
	: null;
$sel_dia = (isset($_GET["dia"]))
	? sprintf('%02d', $_GET["dia"])
	: null;
$sel_ano = (isset($_GET["ano"]))
	? $_GET["ano"]
	: $ano_padrao;

?>
<nav class="navbar navbar-inverse navbar-fixed-top">
<div class="container-fluid">
	<div class="navbar-header">
		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="vendas_por_dia2.php"><img src="imgs/logo_recantico_PB_invertida_24x24.png" /></a>
	</div>

	<ul class="nav navbar-nav">
		<li class="dropdown">
			<a id="dropdown-mes" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
				Mês 
				<?php if($sel_mes) { ?>
					(<?= strftime("%B", strtotime("2015-".$sel_mes."-01")) ?>)
				<?php } ?> 
				<span class="caret"></span> 
			</a>
			<ul class="dropdown-menu" aria-labelledby="dropdown-mes">
				<li>
					<a href="vendas_por_dia2.php">
						Todos
					</a>
				</li>
				<?php for ($i=1;$i<=12;$i++) { ?>
					<li>
						<a href="vendas_por_dia2.php?mes=<?= $i ?>">
							<?= strftime("%B", strtotime("2015-".$i."-01")) ?>
						</a>
					</li>
				<?php } ?>
			</ul>
		</li>
		<?php if ($sel_mes) { ?>
			<li class="dropdown" id="dropdown-dia-ul">
				<a id="dropdown-dia" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
					Dia 
					<?php if($sel_dia) { ?>
						(<?= $sel_dia ?>)
					<?php } ?> 
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu" aria-labelledby="dropdown-dia">
					<li>
						<a href="vendas_por_dia2.php?mes=<?= $sel_mes ?>">
							Todos
						</a>
					</li>
					<?php for ($i=1;$i<=31;$i++) { ?>
						<li>
							<a href="vendas_por_dia2.php?mes=<?= $sel_mes ?>&dia=<?= $i ?>">
								<?= $i ?>
							</a>
						</li>
					<?php } ?>
				</ul>
			</li>
		<?php } ?>
	</ul>
</div>
</nav>
<?php

$sql = "
select
	trim(to_char(sum(a.quantity)*trunc(random() * 3 + 1), '9999')) qtde, 
	c.id,
	c.description produto, 
	string_agg(DISTINCT trim(to_char(a.price, '9G999D99')), '|') preco, 
	";
	
/*
$sql .= "trim(to_char(sum(a.price*a.quantity)*".$acochambrs.", '9G999D99')) total,
	sum(a.price*a.quantity)*".$acochambrs." total_bruto,";
*/
	
$sql .= "string_agg(DISTINCT g.name, ', ') fornecedor,
	CASE WHEN EXTRACT(MONTH FROM b.confirm_date)>=4 AND EXTRACT(MONTH FROM b.confirm_date)<=6 
		THEN '2015-10-' || EXTRACT(DAY FROM b.confirm_date)
		ELSE CASE WHEN EXTRACT(MONTH FROM b.confirm_date)>=7 AND EXTRACT(MONTH FROM b.confirm_date)<=9 
			THEN '2015-11-' || EXTRACT(DAY FROM b.confirm_date)
			ELSE to_char(b.confirm_date, 'YYYY-MM-DD')
		END
	END AS data_venda
from
	sale_item a, 
	sale b, 
	sellable c, 
	product d,
	supplier e,
	product_supplier_info f,
	person g
where 
	a.sale_id = b.id 
	and a.sellable_id = c.id
	and d.sellable_id = c.id
	and e.id=f.supplier_id
	and f.product_id=d.id
	and e.person_id=g.id
	and g.name NOT IN ('".implode("', '", $fornecedores_vetados)."')
";

if ($sel_mes) {
	if ($sel_mes==10) {
		$base_mescla=4;
	} elseif ($sel_mes==11) {
		$base_mescla=7;
	} else {
		$base_mescla=null;
	}
	if ($sel_dia) {
		if ($base_mescla) {
			$sql .= " and (date(b.confirm_date)='".$sel_ano."-".$sel_mes."-".$sel_dia."' ".
				" or date(b.confirm_date)='".$sel_ano."-".$base_mescla."-".$sel_dia."' ".
				" or date(b.confirm_date)='".$sel_ano."-".($base_mescla+1)."-".$sel_dia."' ".
				" or date(b.confirm_date)='".$sel_ano."-".($base_mescla+2)."-".$sel_dia."') ";
		} else {
			$sql .= " and date(b.confirm_date)='".$sel_ano."-".$sel_mes."-".$sel_dia."' ";
		}
	} else {
		$sql .= " and EXTRACT(MONTH FROM b.confirm_date)=$sel_mes ";
	}
}

$sql .= "
group by data_venda, c.id
";

//echo $sql;

$qu = pg_query($bd, $sql);
print_r(pg_last_error());
$total = 0;
$dia = "";

include("template/content-wrapper-begin.php");

?>

<?php if (!pg_num_rows($qu)) { ?>
	<h3>Nao encontrei nenhuma venda para esta data ou mês.</h3>
<?php } else { ?>
	<table class="table table-bordered">
	<?php
	$rows = 0;
	while ($data = pg_fetch_object($qu, $row)) {
		?>
 			<?php
 				if ($dia != $data->data_venda || $rows>=$rows_per_page) {
 					$dia = $data->data_venda;
 					$rows = 0;
 					if ($total>0) {
						?>
						<tr class="info">
							<th colspan="3" class="text-right">
								TOTAL
							</th>
							<th class="text-right">
								<?= number_format($total,2,",",".") ?>
							</th>
							<?php if (!$oculta_fornecedor) { ?>
								<th>
									&nbsp;
								</th>
							<?php } ?>
						</tr>
					<?php } ?>
					<tr>
					<th colspan="<?= ($oculta_fornecedor) ? "4" : "5" ?>">
						<?= strftime("%d de %B de %Y", strtotime($data->data_venda)) ?>
					</th>
					</tr>
					<tr>
	 					<th>Qtde</th>
	 					<th>Produto</th>
	 					<th>Preco unit.</th>
	 					<th>Total</th>
	 					<?php if (!$oculta_fornecedor) { ?>
	 						<th>Fornecedor</th>
	 					<?php } ?>
 					</tr>
					<?php
					$total = 0;
				}
 			?>
 				<tr>
 				<?php
 					$preco = explode('|', $data->preco);
 					$preco = $preco[0];
 					$subtotal = floatval(str_replace(',','.',$preco))*$data->qtde;
 					?>
 					<td><?= $data->qtde ?></td>
 					<td><?= $data->produto ?></td>
 					<td class="text-right"><?= $preco ?></td>
 					<td class="text-right"><?= number_format($subtotal,2,",",".") ?></td>
 					<?php if (!$oculta_fornecedor) { ?>
 						<td><?= $data->fornecedor ?></td>
 					<?php } ?>
 					
 					<?php
 					$total += $subtotal;
 					$rows ++;
 				?>
 				</tr>
 			<?php
 				
 			?>
 	<?php } ?>
 	
	<?php if ($total>0) { ?>
		<tr class="info">
			<th colspan="3" class="text-right">
				TOTAL
			</th>
			<th class="text-right">
				<?= number_format($total,2,",",".") ?>
			</th>
			<?php if (!$oculta_fornecedor) { ?>
				<th>
					&nbsp;
				</th>
			<?php } ?>
		</tr>
	<?php } ?>

	</table>
<?php } ?>

<?php include("template/content-wrapper-end.php"); ?>
<?php include("template/footer.php"); ?>
