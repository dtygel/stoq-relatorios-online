<?php

if (isset($_GET['q'])) {
	$q = $_GET['q'];

	include("config.php");
	include("functions.php");
	setlocale(LC_TIME, "pt_BR.UTF-8");
	include("connect.php");
	
	$sql = "
	select
		c.id, 
		c.description produto, 
		c.base_price preco, 
		c.cost custo, 
		g.name
	from
		sellable c, 
		product d,
		supplier e,
		product_supplier_info f,
		person g
	where 
		d.sellable_id = c.id
		and e.id=f.supplier_id
		and f.product_id=d.id
		and e.person_id=g.id
		and (unaccent(lower(c.description)) like unaccent(lower('".$q."%')) or unaccent(lower(c.description)) like unaccent(lower('% ".$q."%')))
	";

	//echo $sql;

	$qu = pg_query($bd, $sql);
	print_r(pg_last_error());
	$total = 0;
	$dia = "";

	if (!pg_num_rows($qu)) {
		$ret="error";
	} else {
		$rows = 0;
		while ($data = pg_fetch_object($qu, $row)) {
			$t = new StdClass;
			$t->id = $data->id;
			$t->value = $data->produto." (R$".number_format($data->preco,2,',','.')." / R$".number_format($data->custo,2,',','.').")";
			$ret[] = $t;
		}
	}
	header('Content-Type: application/json');
	echo json_encode($ret);
}
?>
