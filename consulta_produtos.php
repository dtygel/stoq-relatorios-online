<?php
	$javascripts = array(
		"consulta-produtos"
	);
	include('template/header.php');
?>
<script src="js/Chart.min.js"></script>
<script src="js/chartjs_product_sales_evolution.js"></script>
<nav class="navbar navbar-inverse navbar-fixed-top">
<div class="container-fluid">
	<div class="navbar-header">
		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="consulta_produtos.php"><img style="float:left" src="imgs/logo_recantico_PB_invertida_24x24.png" /> <span style="padding-left:10px">Consulta online de produtos do Recantico</span></a>
	</div>
</div>
</nav>

<?php include("template/content-wrapper-begin.php"); ?>

<div id="remote">
  <input class="typeahead" type="text" placeholder="buscar...   ">
  <img class="Typeahead-spinner" src="imgs/loading.gif">
</div>
<?php if (isset($_GET['id'])) { ?>
<?php
	$sql = "
	select
		c.id, 
		c.description produto, 
		c.base_price preco, 
		c.cost custo, 
		i.quantity qtde_estoque,
		g.name fornecedor
	from
		sellable c, 
		product d,
		supplier e,
		product_supplier_info f,
		person g,
		storable h
	left join product_stock_item i on i.storable_id = h.id
	where 
		d.sellable_id = c.id
		and e.id=f.supplier_id
		and f.product_id=d.id
		and e.person_id=g.id
		and h.product_id=d.id
		and c.id='".$_GET['id']."'
	";
	$qu = pg_query($bd, $sql);
	//print_r(pg_last_error());
	$total = 0;
	$dia = "";

	if (!pg_num_rows($qu)) {
		pR('error');
		echo $sql;
	} else {
		/* Evolução das vendas */
		$sql = "
		select
			a.quantity,
			a.price,
			date(b.confirm_date) data_venda
		from
			sale_item a,
			sale b
		where 
			a.sale_id = b.id 
			and a.sellable_id='".$_GET['id']."'
		order by
			b.confirm_date DESC
		";
		$qu2 = pg_query($bd, $sql);
		$row2 = 0;
		$total_vendido = 0;
		$vendas = array();
		$vendas_por_mes_desc = array();
		while ($data2 = pg_fetch_object($qu2, $row2)) {
			$t = new stdClass();
			$t->data_venda = strftime("%d/%m/%Y", strtotime($data2->data_venda));
			$t->quantity = $data2->quantity;
			$t->quantity_formatted = number_format($data2->quantity,2,",",".");
			$vendas[] = $t;
			$row2++;
			$total_vendido += $data2->quantity;
			$mes_venda = strftime("%m/%Y", strtotime($data2->data_venda));
			if (!isset($vendas_por_mes_desc[$mes_venda])) {
				$vendas_por_mes_desc[$mes_venda] = 0;
			}
			$vendas_por_mes_desc[$mes_venda] += $data2->quantity;
		}
		$data_primeira_venda = end(array_keys($vendas_por_mes_desc));
		$data_ultima_venda = array_keys($vendas_por_mes_desc)[0];
		$mes_primeira_venda = substr($data_primeira_venda,0,2);
		$mes_ultima_venda = substr($data_ultima_venda,0,2);
		$ano_primeira_venda = substr($data_primeira_venda,3,4);
		$ano_ultima_venda = substr($data_ultima_venda,3,4);
		$vendas_por_mes = array();
		for ($ano = $ano_primeira_venda; $ano<=$ano_ultima_venda; $ano++) {
			$limite_mes = ($ano == $ano_ultima_venda)
				? $mes_ultima_venda
				: 12;
			$inicio_mes = ($ano==2015)
				? 4
				: 1;
			for ($mes = $inicio_mes; $mes<=$limite_mes; $mes++) {
				$d = sprintf("%02d", $mes)."/".$ano;
				$vendas_por_mes[$d] = (isset($vendas_por_mes_desc[$d]))
					? $vendas_por_mes_desc[$d]
					: 0;
			}
		}
		
		/* Evolução do estoque */
		$sql = "
		select 
			a.date, 
			a.quantity, 
			a.type 
		from 
			stock_transaction_history a, 
			product_stock_item b, 
			storable c, 
			product d
		where
			a.product_stock_item_id=b.id 
			and b.storable_id=c.id 
			and c.product_id = d.id
			and d.sellable_id = '".$_GET['id']."'
		";
		$qu3 = pg_query($bd, $sql);
		$row3 = 0;
		$stock = 0;
		$transacoes_unordered = array();
		while ($data3 = pg_fetch_object($qu3, $row3)) {
			$t = new stdClass();
			$stock += $data3->quantity;
			$t->date = strftime("%d/%m/%Y", strtotime($data3->date));
			$t->quantity = $data3->quantity;
			$t->quantity_formatted = number_format($data3->quantity,2,",",".");
			$t->type = $data3->type;
			$t->stock = $stock;
			$transacoes_unordered[$t->date] = $t;
			$row3++;
		}
		$data_primeiro_fato = array_keys($transacoes_unordered)[0];
		$data_ultimo_fato = end(array_keys($transacoes_unordered));
		$dia_primeiro_fato = substr($data_primeiro_fato,0,2);
		$dia_ultimo_fato = substr($data_ultimo_fato,0,2);
		$mes_primeiro_fato = substr($data_primeiro_fato,3,2);
		$mes_ultimo_fato = substr($data_ultimo_fato,3,2);
		$ano_primeiro_fato = substr($data_primeiro_fato,6,4);
		$ano_ultimo_fato = substr($data_ultimo_fato,6,4);
		//pR($data_primeiro_fato);exit;
		$stock_evolution = array();
		$stock_evolution_labels = array();
		$stock = 0;
		for ($ano = $ano_primeiro_fato; $ano<=$ano_ultimo_fato; $ano++) {
			$limite_mes = ($ano == $ano_ultimo_fato)
				? $mes_ultimo_fato
				: 12;
			$inicio_mes = ($ano==2015)
				? 4
				: 1;
			if ($inicio_mes < $mes_primeiro_fato && $ano==$ano_primeiro_fato) {
				$inicio_mes = $mes_primeiro_fato;
			}
			for ($mes = $inicio_mes; $mes<=$limite_mes; $mes++) {
				$limite_dia = ($mes==$limite_mes && $ano==$ano_ultimo_fato)
					? $dia_ultimo_fato
					: cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
				$inicio_dia = ($mes==$inicio_mes && $ano==$ano_primeiro_fato)
					? $dia_primeiro_fato
					: 1;
				for ($dia = $inicio_dia; $dia<=$limite_dia; $dia++) {
					$d = sprintf("%02d", $dia)."/".sprintf("%02d", $mes)."/".$ano;
					$stock_evolution[$d] = (isset($transacoes_unordered[$d]))
						? $transacoes_unordered[$d]->stock
						: $stock;
					if (isset($transacoes_unordered[$d])) {
						$stock = $transacoes_unordered[$d]->stock;
					}
					$stock_evolution_labels[$d] = ($dia==$inicio_dia)
						? $d
						: "";
				}
			}
		}
		//pR($stock_evolution);exit;
		
		
		$rows = 0;
		$data = pg_fetch_object($qu, $row);
			?>
			<h3><?= $data->produto ?></h3>
			<h4><?= $data->did ?></h3>
			<div class="row">
				<div class="col-lg-1 col-sm-12">
					<b>Preço:</b>
				</div>
				<div class="col-lg-11 col-sm-12">
					R$ <?=number_format($data->preco,2,',','.')?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-1 col-sm-12">
					<b>Custo:</b>
				</div>
				<div class="col-lg-11 col-sm-12">
					R$ <?=number_format($data->custo,2,',','.')?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-1 col-sm-12">
					<b>Estoque:</b>
				</div>
				<div class="col-lg-11 col-sm-12">
					<?=number_format($data->qtde_estoque,0)?> unidades
				</div>
			</div>
			<div class="row">
				<div class="col-lg-1 col-sm-12">
					<b>Fornecedor:</b>
				</div>
				<div class="col-lg-11 col-sm-12">
					<?=$data->fornecedor?>
				</div>
			</div>
			
			<h4>Vendas no tempo</h4>
			<canvas id="sellable_sales_evolution" height="250px" width="600px"></canvas>
			<script>
				var data = {
						labels: ["<?= implode('", "', array_keys($vendas_por_mes)) ?>"],
						datasets: [
								{
								    label: "Vendas por mês",
								    fillColor: "rgba(100,100,220,0.2)",
								    strokeColor: "rgba(100,100,220,1)",
								    pointColor: "rgba(100,100,220,1)",
								    pointStrokeColor: "#fff",
								    pointHighlightFill: "#fff",
								    pointHighlightStroke: "rgba(220,220,220,1)",
								    data: [<?= implode(',', $vendas_por_mes) ?>]
								}
						]
				};

				var ctx = $("#sellable_sales_evolution").get(0).getContext("2d");
				var sellableSalesEvolution = new Chart(ctx).Line(data);
			</script>
			
			<h4>Evolução do estoque</h4>
			<canvas id="stock_evolution" height="250px" width="600px"></canvas>
			<script>
				var data = {
						labels: ["<?= implode('", "', $stock_evolution_labels) ?>"],
						datasets: [
								{
								    label: "Evolução do estoque",
								    fillColor: "rgba(100,100,220,0.2)",
								    strokeColor: "rgba(100,100,220,1)",
								    pointHighlightFill: "#fff",
								    pointHighlightStroke: "rgba(220,220,220,1)",
								    data: [<?= implode(',', $stock_evolution) ?>]
								}
						]
				};
				var options = {
					///Boolean - Whether grid lines are shown across the chart
					scaleShowGridLines : true,
					//Boolean - Whether to show horizontal lines (except X axis)
					scaleShowHorizontalLines: true,
					//Boolean - Whether to show vertical lines (except Y axis)
					scaleShowVerticalLines: false,
					//Boolean - Whether the line is curved between points
					bezierCurve : true,
					//Number - Tension of the bezier curve between points
					bezierCurveTension : 0.4,
					//Boolean - Whether to show a dot for each point
					pointDot : false,
					//Boolean - Whether to show a stroke for datasets
					datasetStroke : true,
					//Number - Pixel width of dataset stroke
					datasetStrokeWidth : 2,
					//Boolean - Whether to fill the dataset with a colour
					datasetFill : true,
					// Boolean - Determines whether to draw tooltips on the canvas or not
			    showTooltips: false
			};


				var ctx = $("#stock_evolution").get(0).getContext("2d");
				var stockEvolution = new Chart(ctx).Line(data, options);
			</script>
			
			
			<h4>Todas as vendas</h4>
			<table class="table table-bordered">
			<tr class="info">
				<th>Data</th>
				<th>Quantidade</th>
			</tr>
			<?php
				foreach ($vendas as $venda) {
					?>
					<tr>
						<td><?= $venda->data_venda ?></td>
						<td><?= $venda->quantity_formatted ?></td>
					</tr>
					<?php
				}
			?>
				<tr class="info">
					<th class="text-right">Total vendido</th>
					<th><?= number_format($total_vendido,2,",",".") ?></th>
				</tr>
			</table>
			<?php
	}
?>
<?php } ?>

<?php include("template/content-wrapper-end.php"); ?>
<?php include("template/footer.php"); ?>

