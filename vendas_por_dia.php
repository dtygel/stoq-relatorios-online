<?php

include('template/header.php');

// Define se se faz um aumento randômico das vendas (0 não faz explosão)
$explode = 4;


$sel_ano = (isset($_GET["ano"]))
	? $_GET["ano"]
	: date('Y');
$sel_mes = $sel_dia = null;
if (isset($_GET["mes"])) {
	$sel_mes = sprintf('%02d', $_GET["mes"]);
} elseif (!isset($_GET["ano"])) {
	$sel_mes = date('m');
}
if (isset($_GET["dia"])) {
	$sel_dia = sprintf('%02d', $_GET["dia"]);
} elseif (!isset($_GET["mes"])) {
	$sel_dia = date('d');
}

//pR($sel_ano); pR($_GET['mes']);pR($sel_mes);exit;
	
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
<div class="container-fluid">
	<div class="navbar-header">
		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="vendas_por_dia.php"><img src="imgs/logo_recantico_PB_invertida_24x24.png" /></a>
	</div>

	<ul class="nav navbar-nav">
		<li class="dropdown">
			<a id="dropdown-mes" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
				Ano 
				<?php if($sel_ano) { ?>
					(<?= $sel_ano ?>)
				<?php } ?> 
				<span class="caret"></span> 
			</a>
			<ul class="dropdown-menu" aria-labelledby="dropdown-mes">
				<?php for ($i=$min_year;$i<=$max_year;$i++) { ?>
					<li>
						<a href="vendas_por_dia.php?ano=<?= $i ?>">
							<?= $i ?>
						</a>
					</li>
				<?php } ?>
			</ul>
		</li>
		<li class="dropdown">
			<a id="dropdown-mes" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
				Mês 
				<?php if($sel_mes) { ?>
					(<?= strftime("%B", strtotime("2015-".$sel_mes."-01")) ?>)
				<?php } ?> 
				<span class="caret"></span> 
			</a>
			<ul class="dropdown-menu" aria-labelledby="dropdown-mes">
				<li>
					<a href="vendas_por_dia.php?ano=<?= $sel_ano ?>">
						Todos de <?= $sel_ano ?>
					</a>
				</li>
				<?php for ($i=1;$i<=12;$i++) { ?>
					<li>
						<a href="vendas_por_dia.php?ano=<?= $sel_ano ?>&mes=<?= $i ?>">
							<?= strftime("%B", strtotime("2015-".$i."-01")) ?>
						</a>
					</li>
				<?php } ?>
			</ul>
		</li>
		<?php if ($sel_mes) { ?>
			<li class="dropdown" id="dropdown-dia-ul">
				<a id="dropdown-dia" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
					Dia 
					<?php if($sel_dia) { ?>
						(<?= $sel_dia ?>)
					<?php } ?> 
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu" aria-labelledby="dropdown-dia">
					<li>
						<a href="vendas_por_dia.php?ano=<?= $sel_ano ?>&mes=<?= $sel_mes ?>">
							Todos
						</a>
					</li>
					<?php for ($i=1;$i<=31;$i++) { ?>
						<li>
							<a href="vendas_por_dia.php?ano=<?= $sel_ano ?>&mes=<?= $sel_mes ?>&dia=<?= $i ?>">
								<?= $i ?>
							</a>
						</li>
					<?php } ?>
				</ul>
			</li>
		<?php } ?>
	</ul>
</div>
</nav>
<?php
$sql = "select * from product";
$qu = pg_query($bd, $sql);
pR(pg_fetch_object($qu, $row));
exit;

$sql = "
select";

$sql .= ($explode)
	? "	trim(to_char(sum(a.quantity)*trunc(random() * $explode + 1), '9999')) qtde,"
	: "	trim(to_char(sum(a.quantity), '9999')) qtde,";

$sql .= "
	c.id, 
	string_agg(DISTINCT c.description, ', ') produto, 
	string_agg(DISTINCT trim(to_char(a.price, '9G999D99')), ',') preco, 
	string_agg(DISTINCT g.name, ', ') fornecedor,
	date(confirm_date) data_venda
from
	sale_item a, 
	sale b, 
	sellable c, 
	product d,
	supplier e,
	product_supplier_info f,
	person g
where 
	a.sale_id = b.id 
	and a.sellable_id = c.id
	and d.sellable_id = c.id
	and e.id=f.supplier_id
	and f.product_id=d.id
	and e.person_id=g.id
	and g.name NOT IN ('".implode("', '", $fornecedores_vetados)."')
";

if ($sel_mes) {
	$sql .= ($sel_dia)
		? "and date(b.confirm_date)='".$sel_ano."-".$sel_mes."-".$sel_dia."' " 
		: "and EXTRACT(MONTH FROM b.confirm_date)=$sel_mes ";
}

$sql .= "and EXTRACT(YEAR FROM b.confirm_date)=$sel_ano";


$sql .= "
group by date(b.confirm_date), c.id
";

//pR($sql);exit;

$qu = pg_query($bd, $sql);
print_r(pg_last_error());
$total = 0;
$dia = "";

include("template/content-wrapper-begin.php");

?>

<?php if (!pg_num_rows($qu)) { ?>
	<h3>Nao encontrei nenhuma venda para esta data ou mês.</h3>
<?php } else { ?>
	<table class="table table-bordered">
	<?php
	$rows = 0;
	while ($data = pg_fetch_object($qu, $row)) {
		?>
 			<?php
 				if ($dia != $data->data_venda || $rows>=$rows_per_page) {
 					$dia = $data->data_venda;
 					$rows = 0;
 					if ($total>0) {
						?>
						<tr class="info">
							<th colspan="3" class="text-right">
								TOTAL
							</th>
							<th class="text-right">
								<?= number_format($total,2,",",".") ?>
							</th>
							<?php if (!$oculta_fornecedor) { ?>
								<th>
									&nbsp;
								</th>
							<?php } ?>
						</tr>
					<?php } ?>
					<tr>
					<th colspan="<?= ($oculta_fornecedor) ? "4" : "5" ?>">
						<?= strftime("%d de %B de %Y", strtotime($data->data_venda)) ?>
					</th>
					</tr>
					<tr>
	 					<th>Qtde</th>
	 					<th>Produto</th>
	 					<th>Preco unit.</th>
	 					<th>Total</th>
	 					<?php if (!$oculta_fornecedor) { ?>
	 						<th>Fornecedor</th>
	 					<?php } ?>
 					</tr>
					<?php
					$total = 0;
				}
 			?>
 				<tr>
 				<?php
 					$preco = explode('|', $data->preco);
 					$preco = $preco[0];
 					$subtotal = floatval(str_replace(',','.',$preco))*$data->qtde;
 					?>
 					<td><?= $data->qtde ?></td>
 					<td><?= $data->produto ?></td>
 					<td class="text-right"><?= $preco ?></td>
 					<td class="text-right"><?= number_format($subtotal,2,",",".") ?></td>
 					<?php if (!$oculta_fornecedor) { ?>
 						<td><?= $data->fornecedor ?></td>
 					<?php } ?>
 					
 					<?php
 					$total += $subtotal;
 					$rows ++;
 				?>
 				</tr>
 			<?php
 				
 			?>
 	<?php } ?>
 	
	<?php if ($total>0) { ?>
		<tr class="info">
			<th colspan="3" class="text-right">
				TOTAL
			</th>
			<th class="text-right">
				<?= number_format($total,2,",",".") ?>
			</th>
			<?php if (!$oculta_fornecedor) { ?>
				<th>
					&nbsp;
				</th>
			<?php } ?>
		</tr>
	<?php } ?>

	</table>
<?php } ?>

<?php include("template/content-wrapper-end.php"); ?>
<?php include("template/footer.php"); ?>
